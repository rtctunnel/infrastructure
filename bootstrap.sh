#!/bin/bash
set -x

# TO INSTALL
# 1. Put this in the startup script:
#   curl -L https://bitbucket.org/rtctunnel/infrastructure/raw/master/bootstrap.sh | sudo bash
# 2. Add a role metadata entry that specifies which role to use

main() {
  local role

  # first grab the role from the metdata
  role="$(curl -H 'Metadata-Flavor: Google' http://metadata.google.internal/computeMetadata/v1/instance/attributes/role)"

  cd /tmp || exit
  gsutil cp gs://rtctunnel-pkg/stack/linux_amd64/v0.5.tar.xz ./stack.tar.xz
  tar -xf stack.tar.xz
  ./stack install bitbucket://rtctunnel/infrastructure/roles/$role.yml
}

main
